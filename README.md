# Simple Text Editor for Android

This is very simple text editor that can help you with editing text files. 

This editor mostly used by me for small text notes, write down small notes.

Supports only plain text files at the moment.

The code is open so can review code, send pull requests, new features, translations and so on. 

Github repo: https://github.com/maxistar/TextPad.

Any suggestions, pull requests, translations for this project are welcomed. Thank you!

### Issues Tracker

https://github.com/maxistar/TextPad/issues

### How to run sniffer

./gradlew lint

### Deploy with fastlane

fastlane deploy

### To Do

many many things to do.

### Testing Commits

Update-1
Update-2
Update-4

